# 2012

## January

### 04-05.

####  [4chan final.jpg](2012/01.4chan) was posted on 4chan's /b/ board
* [Imgur Decoy](2012/02.imgur/)

### 04., 17:08:29 UTC
`CageThrottleUs` registered.

#### 17:30:28 UTC
* [Subreddit created](2012/03.subreddit).

#### 18:19:23 UTC
* First Reddit post.

### 05., 03.46.03 UTC
* [KXLOP.jpg](/2012/03.subreddit/reddit#welcome-image) (aka `Welcome`) signed.

#### 04.01.31 UTC
* [KXLOP.jpg](/2012/03.subreddit/reddit#welcome-image) (aka `Welcome`) posted.

### 06., 09:56:47 UTC
* `ImagoOnNib` registered.

### 07., 10.07.51 UTC
* [8D7hN.jpg](/2012/03.subreddit/reddit#problems-image) (aka `Problems?`) signed.

#### 10:27:45  UTC
* [8D7hN.jpg](/2012/03.subreddit/reddit#problems-image) (aka `Problems?`) posted.

### 09., 17:00:00 UTC
* [845145127.com](/2012/03.subreddit/845145127.com) first changed.

### 10., 23:45 UTC
* `sq6wmgv2zcsrix6t.onion` down.

### 11.
* [845145127.com](/2012/03.subreddit/845145127.com) changed.

### 15., 03:45 UTC
* RSA puzzle mails.

### 16.
* Posting number at `sq6wmgv2zcsrix6t.onion/NUM` gives HTTP/500.

## 17., 02:49 UTC
* `sq6wmgv2zcsrix6t.onion/NUM` accepts solutions.

## 23., 16:19:14
MIDI created (according to the metadata in the file).

## February

### 06., 23:27:05 UTC
* [Valēte! image](/2012/03.subreddit/reddit#valēte-image)<br>
* Invitation mails sent.