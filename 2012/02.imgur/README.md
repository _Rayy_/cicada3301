# 2012.02 - Imgur Decoy Duck (m9sYK.jpg)

From the original [final.jpg](../01.4chan) puzzle we extracted an [imgur url](http://i.imgur.com/m9sYK.jpg).

![Decoy Image](img/m9sYK.jpg) 

<details>
    <summary>For searching purposes</summary>
    
    WOOPS
    Just decoys this way.
    
    Looks like you can't guess how to get the message out.
</details>

## Discoveries

1. Hinted to use [OutGuess](http://cimarron.river.com/mirrors/www.outguess.org/) on original [4chan final.jpg](../01.4chan) puzzle.

## [Next Puzzle](../01.4chan)

Go back to the first puzzle and use the hint to run [OutGuess](http://cimarron.river.com/mirrors/www.outguess.org/) against it.

## Referenced In

1. [2012.01 - 4chan 3301.jpg](../01.4chan) which gave us the URL to this image.