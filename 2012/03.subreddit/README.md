# 2012.03 - Subreddit Book Code

Using OutGuess on the original [final.jpg](../01.4chan/img/final.jpg) resulted in a book code with a link to [/r/a2e7j6ic78h0j](https://www.reddit.com/r/a2e7j6ic78h0j)

## [Subreddit](reddit/)

## [Phone Number](phone_number/)

## [845145127.com](845145127.com/)

## [QR Codes](qr_codes/)

## [Two Book Codes (Agrippa and Encyclopedia Brittanica)](two_book_codes/)

## [The Onion](onion/)

## Referenced In

1. [2012.01 4chan final.jpg](../02.imgur) which gave us the hint to use outguess on the image.